# Rowe Family Tree

```mermaid
flowchart LR

subgraph Shepard

    S1("Andrew Parker SHEPPARD, *c.1816 (Tandragee)\nMargretta HAZELTON, ⚭1 Sep. 1847")--> S20(Parker, *c1848\n†drowned)
    S1 --> S21("Jane (Jenny), *c.1849\nJames HERON, ⚭")
    S1 --> S22(Bessie Aileen, *c.1851\nWilliam JENKINS, ⚭)
    S1 --> S23("Mary, *c.1853\nHumphrey SEMPLE, ⚭, †1891 (lost at sea)")
    S1 --> S24("Robert Hazelton, *10 May 1855\nMary Seaby CLARIDGE ⚭c.1886 (NZ)")
    S1 --> S25("Margaretta (Metta), *20 Mar 1856\n†1953/4 (age 96-98)")
    S1 --> S26(William, *17 Nov. 1858\n†Lurgan, Armagh)
    S1 --> S27(Herbert, 27 Jul. 1861\n†Belfast)


    S24 --> S31(Robert James Herbert, * 8 Oct. 1889, †25 Dec. 1974\nHelen HILL, *16 Jun. 1887, †1981)
    S24 --> S32("Roderick Hazelton, *21 Feb. 1895\nDorothy Dulcie MOORE, *5 Aug. 1898 (Opunake), ⚭9 Jun. 1926 (Napier), †21 Jun. 1974 (Papakura)")
    S24 --> S33("Olive Seaby Knox (née SHEPPARD), *24 Jul. 1897, †10 Dec. 1975")
end
S24 --> S30("Thomas ROWE, *c1878, ⚭1912, †23 Sep 1957\nMargaretta Mary, *4 July 1888, †c1966 (Sidcup, Kent Eng.)")

    R1("John ROW, *1787\nMary Martin, ⚭20 Nov. 1815") --> A2("John, *1820")
    R1 --> R20("Mary, *1822")
    R1 --> R21("Thomas, *1825")
    R1 --> R22("Richard, *1827\nFanny ROWE, ⚭1852 ")
    R1 --> R23("Sophia, *1834")

    %% GENERATION 2 %%

    R22 --> R30("John, *1852")
    R22 --> A8("Richard, *1854")
    R22 --> A9("Thomas, *1871")

    %% GENERATION 3 %%

    R30 --> R41("Mary, *1878")
    R30 --> R42("Laura, *1879")
    R30 -->R43("Fanny C., *1881\n†1883")
    R30 -->R44("Grace, *1883")
    R30 --> R45("Elizabeth, *1884")

    R30 -->S30


    S30 --> C1(John Sheppard, *14 Jul. 1913\nLillian Amelia STONE, ⚭14 Oct 1939, †3 Jun. 1978)
    S30 --> C2(Elizabeth Sheppard, *4 Mar. 1915\nAllenson G. RUTTER)
    S30 --> C3(Robert Sheppard, *14. May 1917\nPauline Ethel ASHTON, ⚭16 Mar. 1940)
    S30 --> C4(Roderick Sheppard, *7 Oct. 1919\nDorothy - Ming MENZIES, ⚭c.1950\nBarbara CAMERON, ⚭1982)
    C1 --> D1(Desmond John, *10 Jan. 1946\nHelen GIBSON, ⚭c.1968)
    C1 --> D2(Elizabeth, * 4 Oct. 1947\n Timothy KITCHEN, ⚭?\nEarle SPENCER, ⚭16 December 1983, †2 January 1991)
    C2 --> D3(David, *?)
    C2 --> D4(Roderick Peter, *?)
    C2 -->D5(Rosalind, *1944)
    C3 --> D6("George Raymond Ashton, *6 Apr. 1943 (Napier), †4 Oct. 2012\nLois Rae PRYOR, *07 Oct. 1945, ⚭25 Jan. 1969 ")
    C3 --> D7("Patricia May, *25 Sep. 1944 (Napier)\nJoe Beavis")
    C3 --> D8("Thomas Robert, *14 Apr. 1946 (Napier)\nJane Kirstene STOW, *16 Feb. 1960, ⚭1977?")
    C4 --> D9(Peter, *c.1952\nJudy WOODARD, ⚭?)
    C4 --> D10("Beverly, *?\nGraham COTTON, ⚭Jun. 1981 (England)")
    D1 --> E2("Julian Edward, *6 Sep. 1974\nJillian WATSON")
    D1 --> E3("Suzanna, *8 Jul. 1981\nKurt MacBeth *1978, ⚭27 Apr. 2013")
    D2 -->E10("Christopher ROWE-SPENCER *25 Jun. 1988\nElianna APOSTOLIDES, ⚭14 October 2017")

    subgraph Rowe-Spencer
    E10 -->F10("Persephone *23 July 2019")
    E10 -->F11("Arion *18 April 2022")
    D2 -->E9("May ROWE-SPENCER *24 May 1984")

    end

    D1 --> E1("Michelle Catherine, *5 Mar. 1972\nBryce James READ *03 Sept. 1966, ⚭15th March 2003")

    subgraph Read
    E1 --> F6("Emma Charlotte *15 Jun. 2009")
    E1 --> F7("Georgia Grace *21 Mar. 2012")
    end

    D8 --> E4("Eric John, *14 Jul. 1978\nNaomi IRWIN, * , ⚭2001\nTanya KERSWELL,*4 Jan. 1986, ⚭21 Sep. 2013")
    D8 --> E5("Owen Robert, *17 Sep. 1981\nKerstin RUSKOWSKI, *30 Sep. 1983")
    D8 --> E6("Kent Malcolm, *12 Apr. 1983\n Tina")
    D9 --> E11("Rochelle, *\nMatt HANSEN, ⚭20 Feb. 2021")
    D9 --> E12("James")
    D6 --> E7("Cushla Raewyn *16 Jan. 1974\nVaughn Arthur MAJENDIE, ⚭14 Sep 1999")
    D6 --> E8("Anita Loise, *09 Nov. 1976\nTimothy Sean Roy CLARK, ⚭15 Aug 1998")

    subgraph Hansen
    E11 -->F15("Nieva *28 Mar. 2019")
    E11 -->F16("Luca, *7 Oct 2021")
    end

    subgraph Clark
    E8 -->F12("Thomas Isaac, *23 Sep 2003")
    E8 -->F13("Laura Louise, *14 Oct 2007")
    end

    E2 -->F8("Hamish William *26th Mar. 2004")
    E2 -->F9("Megan Jade *6th Oct. 2006")
    E4 --> F1("Caitlin, *18 Dec. 2002")
    E4 --> F2("Alyssa, *11 Feb. 2016")
    E4 --> F3("Kyle, *13 Jul. 2020")
    subgraph Ruskowski
    E5 -->F14("Luisa Agi Jane *12 Jan. 2024")
    end
    E6 --> F4("Amara, *18 Nov. 2011")
    E6 --> F5("Luca, *19 Aug. 2016")


```
